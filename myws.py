import threading

from SimpleWebSocketServer import SimpleWebSocketServer, WebSocket

from ConnectionCallback import ConnectionCallback
import logging

clients = []

connection_callback_list = []
logger = logging.getLogger()

class SimpleChat(WebSocket):

    def sendAll(self, msg):

        for client in clients:
            client.sendMessage(msg)

    def handleConnected(self):
        for client in clients:
            client.sendMessage(self.address[0] + u' - connected')
        clients.append(self)
        for callback in connection_callback_list:
            callback.made(self.address[0])

    def handleClose(self):
        clients.remove(self)
        for client in clients:
            client.sendMessage(self.address[0] + u' - disconnected')
        for callback in connection_callback_list:
            callback.lost(self.address[0])


def start_server(host, port):
    server = SimpleWebSocketServer(host, port, SimpleChat)
    server.serveforever()


def send_to_all(msg):
    logger.info(f"WS Client {len(clients)}")
    for callback in connection_callback_list:
        callback.send(msg)
    for client in clients:
        client.sendMessage(msg)
        client.sendMessage("data")


def add_connection_callback(callback: ConnectionCallback):
    connection_callback_list.append(callback)


def remove_connection_callback(callback: ConnectionCallback):
    connection_callback_list.remove(callback)


def start(host, port):
    threading.Thread(target=start_server, args=(host, port,)).start()
