import asyncio
from typing import Optional
import logging

from ConnectionCallback import ConnectionCallback

connect_list = []
connection_callback_list = []
logger = logging.getLogger()

class EchoServerProtocol(asyncio.Protocol):

    def __init__(self):
        super(EchoServerProtocol, self).__init__()
        self.transport = None
        self.made_callback = None
        self.lost_callback = None

    def connection_made(self, transport):
        self.transport = transport
        peername = self.transport.get_extra_info('peername')
        connect_list.append(self)
        for callback in connection_callback_list:
            callback.made(peername)

    def data_received(self, data: bytes) -> None:
        logger.info(f"TCP Client {len(connect_list)}")
        message = data.decode()
        for callback in connection_callback_list:
            callback.rev(message)

    def connection_lost(self, exc: Optional[Exception]) -> None:
        peername = self.transport.get_extra_info('peername')
        connect_list.remove(self)
        for callback in connection_callback_list:
            callback.lost(peername)


def add_connection_callback(callback: ConnectionCallback):
    connection_callback_list.append(callback)


def remove_connection_callback(callback: ConnectionCallback):
    connection_callback_list.remove(callback)


async def start_tcp_server(port):
    loop = asyncio.get_running_loop()
    server = await loop.create_server(lambda: EchoServerProtocol(), '0.0.0.0', port)
    await server.start_serving()
    return server


if __name__ == "__main__":
    asyncio.run(start_tcp_server())
