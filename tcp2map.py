import asyncio
import json
import logging

import tcp_server
import myws
from ConnectionCallback import ConnectionCallback
import pymap3d

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

tcp_port = 9999
ws_port = 9998

load_f = open("./init.json", 'r')
load_dict = json.load(load_f)
floor_info = load_dict["floorInfo"]
load_f.close()


class MyTCPConnectionCallback(ConnectionCallback):
    def made(self, peername):
        logger.info(f"TCP [MADE] {peername},current tcp connect is {len(tcp_server.connect_list)}")

    def lost(self, peername):
        logger.info(f"TCP [LOST] {peername},current tcp connect is {len(tcp_server.connect_list)}")

    def rev(self, msg):
        dic = json.loads(msg)

        if "type" in dic:
            msg_dic = {"id": dic["tagid"]}
            if dic["type"] == 1:
                tag = dic["tag"]
                origin = dic["origin"]
                p = enu_2_lla(tag["x"], tag["y"], tag['z'], origin["x"], origin["y"], origin['z'])
                msg_dic["lat"] = p[0]
                msg_dic["lon"] = p[1]
                msg_dic["alt"] = p[2]

                if main:
                    msg = get_location_data(p[1], p[0], p[2])
                    myws.send_to_all(msg)
            elif dic["type"] == 2:
                p = dic["tag"]
                msg_dic["lat"] = p["y"]
                msg_dic["lon"] = p["x"]
                msg_dic["alt"] = p["z"]
                if main:
                    msg = get_location_data(p["x"], p["y"], p["z"])
                    myws.send_to_all(msg)


def lla_2_xyz(lat, lon, h):
    return pymap3d.geodetic2ecef(lat, lon, h)


def xyz_2_lla(x, y, z):
    return pymap3d.ecef2geodetic(x, y, z)


def lla_2_enu(lat0, lon0, h0, lat1, lon1, h1):
    return pymap3d.geodetic2enu(lat1, lon1, h1, lat0, lon0, h0)


def enu_2_lla(e, n, u, x0, y0, z0):
    lla = xyz_2_lla(x0, y0, z0)
    return pymap3d.enu2geodetic(e, n, u, lla[0], lla[1], lla[2])


def get_location_data(x, y, z):
    result = bytearray([211, 157, 136])
    scale = 10000000
    xs = int(x * scale)
    ys = int(y * scale)
    xsb = xs.to_bytes(4, byteorder="big")
    ysb = ys.to_bytes(4, byteorder="big")
    result.extend(xsb)
    result.extend(ysb)
    f = None
    for k in floor_info:
        if not f:
            f = floor_info[k]
        if z < int(k):
            f = floor_info[k]
    result.extend(f.to_bytes(2, byteorder="big"))
    return result


def cal_coordinates(origin, ap_list, name_list=None):
    origin_xyz = lla_2_xyz(origin[0], origin[1], origin[2])
    logger.info(f"原点的 xyz 坐标为 ： {origin_xyz}")
    ret = [], [], []
    for i in range(len(ap_list)):
        ap = ap_list[i]
        ap_enu = lla_2_enu(origin[0], origin[1], origin[2], ap[0], ap[1], ap[2])
        name = i
        if name_list:
            ret[0].append(name_list[i])
            ret[1].append(ap_enu[0])
            ret[2].append(ap_enu[1])
            name = name_list[i]
        logger.info(f"基站 {name} 的 东北天坐标为 ： {ap_enu}")
    return ret


class MyWsConnectionCallback(ConnectionCallback):
    def made(self, peername):
        logger.info(f"WS  [MADE] {peername},current tcp connect is {len(myws.clients)}")

    def lost(self, peername):
        logger.info(f"WS  [LOST] {peername},current tcp connect is {len(myws.clients)}")

    def send(self, msg):
        logger.info(f"WS [SEND] {msg}")
        # myws.send_to_all(msg)


def input_tcp_port():
    global tcp_port
    tcp_port_str = input("tcp port [default 9999 , 500 ~ 10000] : ")
    if tcp_port_str == '':
        pass
    else:
        try:
            tcp_port_int = int(tcp_port_str)
            if 500 < tcp_port_int < 10000:
                tcp_port = tcp_port_int
        except ValueError:
            pass
    logger.info(f"tcp port set to {tcp_port}")


def input_ws_port():
    global ws_port
    ws_port_str = input("Ws  port [default 9998 ,  500 ~ 10000] : ")
    if ws_port_str == '':
        pass
    else:
        try:
            ws_port_int = int(ws_port_str)
            if tcp_port != ws_port_int and 500 < ws_port_int < 10000:
                ws_port = ws_port_int
        except ValueError:
            pass
    logger.info(f"Ws  port set to {ws_port}")


def start_tcp_server():
    pass


def start_ws_server():
    pass


async def main():
    tcpserver = await tcp_server.start_tcp_server(tcp_port)
    await tcpserver.wait_closed()


if __name__ == "__main__":
    input_tcp_port()
    input_ws_port()
    tcp_server.add_connection_callback(MyTCPConnectionCallback())
    myws.add_connection_callback(MyWsConnectionCallback())
    myws.start('0.0.0.0', ws_port)
    asyncio.get_event_loop().run_until_complete(main())
