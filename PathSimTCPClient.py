import math
import socket
import time
import json

if __name__ == "__main__":
    print("本程序不检查错误，请保证输入正确")
    host = input("Input host [Default 'localhost']: ")
    if host == "":
        host = "localhost"
    print(f"Host = {host}")
    port = input("Input port [Default 9999]")
    if port == "":
        port = 9999
    else:
        port = int(port)
    print(f"Port = {port}")
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect((host, port))
    i = 0
    while True:
        iMod = i % 12
        r = (i / 6) * 0.00001 + 0.000002
        i += 1
        time.sleep(1)
        data = {"version": "1.2", "type": 2, "tagid": 1,
                "tag": {"x": 103.53351 + math.sin(iMod / 12 * 2 * math.pi) * r,
                        "y": 31.03264 + math.cos(iMod / 12 * 2 * math.pi) * r, "z": 912 + i / 10}}
        # data = {"version": "1.2", "type": 2, "tagid": 1,
        #         "tag": {"x": 103.53351,
        #                 "y": 31.03264, "z": 912}}
        if i > 600:
            i = 0
        client.send(json.dumps(data).encode())
